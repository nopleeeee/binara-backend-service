/* eslint-disable prefer-arrow-callback */
/* eslint-disable padded-blocks */
/* eslint-disable no-trailing-spaces */
const product = require('../controllers').product;
const cart = require('../controllers').cart;
module.exports = (app) => {

  app.get('/api', (req, res) => res.status(200).send({
    message: 'Welcome to the Binara API Service!',
  }));

  app.post('/product', product.create);
  app.get('/product', product.list);
  app.get('/product/:id', product.retrieve);
  app.put('/product/:id', product.update);
  app.delete('/product/:id', product.destroy);
  app.all('/product/:id/items', (req, res) => res.status(405).send({
    message: 'Method Not Allowed',
  }));

  app.post('/cart', cart.create);
  app.get('/cart', cart.list);
  app.get('/cart/:id', cart.retrieve);
  app.put('/cart/:id', cart.update);
  app.delete('/cart/:id', cart.destroy);
  app.all('/cart/:id/items', (req, res) => res.status(405).send({
    message: 'Method Not Allowed',
  }));

  app.put('/checkout/:id', cart.checkout);
  app.get('/order-list', cart.orderList);

};
