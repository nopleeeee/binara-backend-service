/* eslint-disable new-cap */
module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define('Product', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    photo: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    category: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    stock: {
      type: DataTypes.INTEGER,
    },
    price: {
      type: DataTypes.INTEGER,
    },
    createdDate: {
      type: DataTypes.DATE,
    },
    modifiedDate: {
      type: DataTypes.DATE,
    },
  }, { timestamps: false });
  Product.associate = (models) => {
    Product.belongsTo(models.Cart, {
      foreignKey: 'id',
      onDelete: 'CASCADE',
    });
  };
  return Product;
};
