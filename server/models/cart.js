/* eslint-disable new-cap */
module.exports = (sequelize, DataTypes) => {
  const Cart = sequelize.define('Cart', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    productId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    quantity: {
      type: DataTypes.INTEGER,
    },
    isCheckout: {
      type: DataTypes.BOOLEAN,
    },
    createdDate: {
      type: DataTypes.DATE,
    },
    modifiedDate: {
      type: DataTypes.DATE,
    },
  }, { timestamps: false });
  Cart.associate = (models) => {
    Cart.hasOne(models.Product, {
      foreignKey: 'id',
      sourceKey: 'productId',
      as: 'products',
    });
  };
  return Cart;
};
