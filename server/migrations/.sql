CREATE DATABASE binara

-- SEQUENCE: public.product_seq

-- DROP SEQUENCE public.product_seq;

CREATE SEQUENCE public.product_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 999999999999
    CACHE 1;

ALTER SEQUENCE public.product_seq
    OWNER TO postgres;

-- SEQUENCE: public.cart_seq

-- DROP SEQUENCE public.cart_seq;

CREATE SEQUENCE public.cart_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.cart_seq
    OWNER TO postgres;

-- Table: public.Products

-- DROP TABLE public."Products";

CREATE TABLE public."Products"
(
    id integer NOT NULL DEFAULT nextval('product_seq'::regclass),
    name character varying COLLATE pg_catalog."default",
    category character varying COLLATE pg_catalog."default",
    photo character varying COLLATE pg_catalog."default",
    stock integer,
    price numeric,
    "createdDate" timestamp with time zone,
    "modifiedDate" timestamp with time zone,
    CONSTRAINT "Products_pkey" PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public."Products"
    OWNER to postgres;

-- Table: public.Carts

-- DROP TABLE public."Carts";

CREATE TABLE public."Carts"
(
    id integer NOT NULL DEFAULT nextval('cart_seq'::regclass),
    quantity integer,
    "productId" integer,
    "isCheckout" boolean,
    "createdDate" timestamp with time zone,
    "modifiedDate" timestamp with time zone,
    CONSTRAINT "Cart_pkey" PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public."Carts"
    OWNER to postgres;