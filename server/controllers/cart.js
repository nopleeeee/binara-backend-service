/* eslint-disable array-callback-return */
const Cart = require('../models').Cart;

module.exports = {
  create(req, res) {
    return Cart
      .create({
        productId: req.body.productId,
        quantity: req.body.quantity,
        isCheckout: false,
        createdDate: new Date(),
      })
      .then((result) => res.status(201).send(result))
      .catch((error) => res.status(400).send(error));
  },

  list(req, res) {
    return Cart
      .findAll({ where: { isCheckout: false }, include: ['products'] })
      .then((result) => res.status(200).send(result))
      .catch((error) => res.status(400).send(error));
  },

  orderList(req, res) {
    return Cart
      .findAll({ where: { isCheckout: true }, include: ['products'] })
      .then((result) => {
        let total = 0;
        result.map(i => {
          total += (Number(i.dataValues.products.price) * Number(i.dataValues.quantity));
        });
        const resp = {
          result: [{ total_price: total, order_list: result }],
        };
        res.status(200).send(resp);
      })
      .catch((error) => res.status(400).send(error));
  },

  retrieve(req, res) {
    return Cart
      .findByPk(req.params.id)
      .then((todo) => {
        if (!todo) {
          return res.status(404).send({
            message: 'Cart Not Found',
          });
        }
        return res.status(200).send(todo);
      })
      .catch((error) => res.status(400).send(error));
  },

  update(req, res) {
    return Cart
      .findByPk(req.params.id)
      .then(todo => {
        if (!todo) {
          return res.status(404).send({
            message: 'Cart Not Found',
          });
        }
        return todo
          .update({
            productId: req.body.productId || todo.productId,
            quantity: req.body.quantity || todo.quantity,
            isCheckout: req.body.isCheckout || todo.isCheckout,
            createdDate: req.body.createdDate || todo.createdDate,
            modifiedDate: new Date(),
          })
          .then(() => res.status(200).send(todo))
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  },

  checkout(req, res) {
    return Cart
      .findByPk(req.params.id)
      .then(todo => {
        if (!todo) {
          return res.status(404).send({
            message: 'Cart Not Found',
          });
        }
        return todo
          .update({
            isCheckout: req.body.isCheckout || todo.isCheckout,
            modifiedDate: new Date(),
          })
          .then(() => res.status(200).send(todo))
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  },

  destroy(req, res) {
    return Cart
      .findByPk(req.params.id)
      .then(todo => {
        if (!todo) {
          return res.status(400).send({
            message: 'Cart Not Found',
          });
        }
        return todo
          .destroy()
          .then(() => res.status(204).send())
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  },
};
