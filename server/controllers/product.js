const Product = require('../models').Product;

module.exports = {
  create(req, res) {
    return Product
      .create({
        name: req.body.name,
        category: req.body.category,
        photo: req.body.photo,
        stock: req.body.stock,
        price: req.body.price,
        createdDate: new Date(),
      })
      .then((result) => res.status(201).send(result))
      .catch((error) => res.status(400).send(error));
  },

  list(req, res) {
    return Product
      .findAll()
      .then((result) => res.status(200).send(result))
      .catch((error) => res.status(400).send(error));
  },

  retrieve(req, res) {
    return Product
      .findByPk(req.params.id)
      .then((todo) => {
        if (!todo) {
          return res.status(404).send({
            message: 'Product Not Found',
          });
        }
        return res.status(200).send(todo);
      })
      .catch((error) => res.status(400).send(error));
  },

  update(req, res) {
    return Product
      .findByPk(req.params.id)
      .then(todo => {
        if (!todo) {
          return res.status(404).send({
            message: 'Product Not Found',
          });
        }
        return todo
          .update({
            name: req.body.name || todo.name,
            category: req.body.category || todo.category,
            photo: req.body.photo || todo.photo,
            stock: req.body.stock || todo.stock,
            price: req.body.price || todo.price,
            createdDate: req.body.createdDate || todo.createdDate,
            modifiedDate: new Date(),
          })
          .then(() => res.status(200).send(todo))
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  },

  destroy(req, res) {
    return Product
      .findByPk(req.params.id)
      .then(todo => {
        if (!todo) {
          return res.status(400).send({
            message: 'Product Not Found',
          });
        }
        return todo
          .destroy()
          .then(() => res.status(204).send())
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  },
};
